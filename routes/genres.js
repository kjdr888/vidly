const express = require('express');
const router = express.Router();

const genres = [
    { id: 1, name: 'Action' },
    { id: 2, name: 'Horror' },
    { id: 3, name: 'Romance' },
];

function validateGenres(genre) {
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    });

    return schema.validate(genre);
}

router.get('/', (req, res) => {
    res.send(genres);
});

router.get('/:id', (req, res) => {
    const genre = genres.find(g => g.id === parseInt(req.params.id))
    if (!genre) return res.status(404).send(`Genre with id:${req.params.id} not found!`);
    res.send(genre);
});

router.post('/', (req, res) => {
    //check if req.body.name is valid
    const { error } = validateGenres(req.body);

    if (error) { //error 400
        return res.status(400).send(error.details[0].message);
    }

    const genre = {
        id: genres.length + 1,
        name: req.body.name
    };

    genres.push(genre);
    res.send(genres);
});

router.post('/', (req, res) => {
    //check if req.body.name is valid
    const { error } = validateGenres(req.body);

    if (error) { //error 400
        return res.status(400).send(error.details[0].message);
    }

    const genre = {
        id: genres.length + 1,
        name: req.body.name
    };

    genres.push(genre);
    res.send(genres);
});

router.put('/:id', (req, res) => {
    const genre = genres.find(g => g.id === parseInt(req.params.id))
    if (!genre)
        return res.status(404).send(`Genre with id:${req.params.id} not found!`);

    //check if req.body.name is valid
    const { error } = validateGenres(req.body);

    if (error)
        return res.status(400).send(error.details[0].message);

    genre.name = req.body.name;

    res.send(genre);
});

router.delete('/:id', (req, res) => {
    const genre = genres.find(g => g.id === parseInt(req.params.id))
    if (!genre) return res.status(404).send(`Genre with id:${req.params.id} not found!`);

    const index = genres.indexOf(genre);
    genres.splice(index);

    res.send(genres);
});

module.exports = router;